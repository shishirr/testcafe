const report = require('multiple-cucumber-html-reporter');
if (process.env.NODE_ENV !== 'production') {
  const result = require('dotenv').config();
  if (result.error) {
    throw result.error;
  }
}
const appName = 'FedHR Navigator';
const appVersion = process.env.APP_VERSION;
const reportGenerationTime =
  new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString();

report.generate({
  reportName: 'FedHR Navigator E2E Test Report',
  jsonDir: 'reports',
  reportPath: 'reports/html',
  openReportInBrowser: true,
  disableLog: false,
  displayDuration: true,
  displayReportTime: true,
  durationInMS: true,
  customData: {
    title: 'Run info',
    data: [
      { label: 'Application', value: `${appName}` },
      { label: 'Version', value: `${appVersion}` },
      { label: 'Report Generation Time', value: `${reportGenerationTime}` },
    ],
  },
});
