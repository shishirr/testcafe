# FedHR Navigator e2e tests

- copy `.env-sample` file as save as `.env`. Edit to set the configuration for the environment to test
- Type `yarn` to install the node modules.
- `yarn run e2e:live` to run all tests in live mode.
  - The browsers remains active while you work on tests. You can see test results instantly because the tests are restarted when you make changes
- `yarn run e2e` Run all tests in non interactive mode
- `yarn run e2e:report` - Run test and generate a HTML report. The generated report is saved at `reports/html/index.html`

# Creating Page Objects

1. create page object models under `src/page-objects/` or if your're modeling only a section of the page, create it under `src/page-objects/partials/`

**Note**: Do not use `t.expect(...)` or other test assertions in page object class. The assertions should be done in test cases

Here is a sample page object.

```javascript
// File: src/page-objects/pageNameUnderTest.ts

import { Selector, t } from '../config';

class PageNameUnderTest {
  private elementOne = Selector('....');
  private elementTwo = Selector('....');
  private elementThree = Selector('....');
  private elementFour = Selector('....');
  private submitButton = Selector('....');

  getElementOne(text: string) {
    return this.elementOne.withText(text);
  }

  async clickElementTwo() {
    await t.click(this.elementTwo);
  }

  async fillOutFormAndSubmit(param3Value: string, param4Value: string) {
    await t
      .typeText(this.elementThree, param3Value, { replace: true })
      .typeText(this.elementFour, param4Value, { replace: true })
      .click(this.submitButton);
  }
}

export const pageNameUnderTest = new PageNameUnderTest();
```

2. Add the page exported page object to `src/page-objects/index.js`

```javascript
// File: src/page-objects/index.js

export * from './pageNameUnderTest';
```

# Creating Test Case

1. Test cases should be created under `src/tests/...`
