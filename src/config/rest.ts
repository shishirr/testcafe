/* eslint-disable @typescript-eslint/no-explicit-any */
import * as http from 'http';
import * as https from 'https';
import * as url from 'url';
import { getBaseUrl } from './index';

const adapters = {
  'http:': http,
  'https:': https,
};

const requestAdapter = adapters[url.parse(getBaseUrl()).protocol];

export const GET = (inputUrl: string): Promise<any> =>
  new Promise<any>((resolve, reject) => {
    requestAdapter
      .get(getBaseUrl() + inputUrl, (res) => {
        const { statusCode } = res;
        const contentType = res.headers['content-type'];
        res.setEncoding('utf8');
        let rawData = '';
        res.on('data', (chunk) => {
          rawData += chunk;
        });
        res.on('end', () => resolve({ statusCode, contentType, rawData }));
      })
      .on('error', (e) => reject(e));
  });
