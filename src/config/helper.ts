import { ClientFunction } from 'testcafe';

export const getLocationPart = ClientFunction((locationPart) => {
  return window.location[locationPart];
});

export const getWindowTitle = ClientFunction(() => document.title);
