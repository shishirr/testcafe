import { Role } from 'testcafe';
import { leftNavigationPane, loginPage, selectRole } from '../page-objects';
import { getLocationPart, getLoginUrl, getWindowTitle, User } from './index';

export function role(tenant: string, userJsonPath: string): Role {
  const dataFolder = process.env.DATA_FOLDER;

  const user: User = User.check(
    require(`../../${dataFolder}/${tenant}/${userJsonPath}`)
  );

  const url = getLoginUrl(tenant);

  return Role(
    url,
    async (t) => {
      await t
        .expect(getLocationPart('pathname'))
        .eql('/frbweb/logon.do')
        .expect(getWindowTitle())
        .eql('FedHR: Login');
      await loginPage.login(user.username, user.password);
      await t.expect(getWindowTitle()).contains('FedHR: Select a Role');
      await selectRole.clickRole(user.role);
      await t.expect(getWindowTitle()).eql('FedHR: Home');
      leftNavigationPane.clickLink('Home');
      await t.expect(getWindowTitle()).eql('FedHR: Home');
    },
    { preserveUrl: true }
  );
}
