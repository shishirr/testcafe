if (process.env.NODE_ENV !== 'production') {
  const result = require('dotenv').config();
  if (result.error) {
    throw result.error;
  }
}

const baseUrl = process.env.BASE_URL || 'http://localhost:8080';
console.debug('APP_VERSION', process.env.APP_VERSION);

export function getBaseUrl(): string {
  return baseUrl;
}

export function getLoginUrl(tenant: string): string {
  return `${baseUrl}/frbweb/logon.do?operation=index&client=${tenant}`;
}
