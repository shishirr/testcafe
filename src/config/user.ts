import { Record, Static, String } from 'runtypes';

export const User = Record({
  username: String,
  password: String,
  role: String,
});

export type User = Static<typeof User>;
