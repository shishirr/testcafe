import { Selector } from 'testcafe';

class CaseTrackingList {
  private pane = Selector('#caseTrackingPageTitle');
  private tabList = Selector('#tabList');

  paneTitle(text: string) {
    return this.pane.withText(text);
  }

  tabName(text: string) {
    return this.tabList.find('span').withText(text);
  }
}

export const caseTrackingList = new CaseTrackingList();
