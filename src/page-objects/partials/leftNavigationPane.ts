import { Selector, t } from 'testcafe';

class LeftNavigationPane {
  private navLinks = Selector('#nav a');

  async clickLink(linkText: string) {
    await t.click(this.navLinks.withExactText(linkText));
  }
}

export const leftNavigationPane = new LeftNavigationPane();
