import { Selector, t } from 'testcafe';

class Login {
  private usernamePasswordTab = Selector('#btn_login_username');
  private username = Selector('#username');
  private password = Selector('#password');
  private submitButton = Selector('#upform > button');

  async login(username: string, password: string) {
    await t
      .click(this.usernamePasswordTab)
      .typeText(this.username, username, { replace: true })
      .typeText(this.password, password, { replace: true })
      .click(this.submitButton);
  }
}

export const loginPage = new Login();
