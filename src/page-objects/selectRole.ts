import { Selector, t } from 'testcafe';

class SelectRole {
  roles = Selector('#rolesSelection a');

  async clickRole(roleName: string) {
    await t.click(this.roles.withText(roleName));
  }
}

export const selectRole = new SelectRole();
