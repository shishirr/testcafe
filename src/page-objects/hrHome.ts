import { Selector } from '../config';

class HrHomePage {
  private paneTitleSelector = Selector('#maincolumnfixed >h2');

  paneTitle(text: string) {
    return this.paneTitleSelector.withText(text);
  }
}

export const hrHomePage = new HrHomePage();
