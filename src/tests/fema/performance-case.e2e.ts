import { role } from '../../config';
import { caseTrackingList, leftNavigationPane } from '../../page-objects';

const tenant = 'FEMA';
const user16254: Role = role(tenant, 'user16254-Administrator.json');

fixture`${tenant}: Case tracking`;

/**
 * Open Benefits cases
 */
test('List Benefits cases', async (t) => {
  const caseType = 'Benefits';

  await leftNavigationPane.clickLink(caseType);

  await t
    .expect(caseTrackingList.paneTitle(caseType).exists)
    .ok()
    .expect(caseTrackingList.tabName('Assigned to Me').exists)
    .ok();
})
  .before(async (t) => {
    await t.useRole(user16254);
  })
  .meta('tenant', tenant);

/**
 * Open retirement cases
 */
test('List Retirement cases', async (t) => {
  const caseType = 'Retirement';

  await leftNavigationPane.clickLink(caseType);

  await t
    .expect(caseTrackingList.paneTitle(caseType).exists)
    .ok()
    .expect(caseTrackingList.tabName('Assigned to Me').exists)
    .ok();
})
  .before(async (t) => {
    await t.useRole(user16254).wait(1);
  })
  .meta('tenant', tenant);
