import { GET, JSONPath } from '../config';

/**
 *  Check build version
 */
fixture`FedHR Navigator Build`;
const expectedVersion = process.env.APP_VERSION;

test(`Is the server running version ${expectedVersion}`, async (t) => {
  const response = await GET('/frbweb/fhrnavigator/version');
  await t.expect(response.statusCode).eql(200);
  const json = JSON.parse(response.rawData);
  const runningVersion = JSONPath({ path: '$.Application-Version', json });

  await t.expect(runningVersion).eql([expectedVersion]);
});

/**
 * check the application component status
 */
[
  'DATABASE',
  'CALC_ENGINE',
  'FILE_SERVICE',
  'EMAIL',
  'CLAM_AV',
  'FOP_SERVICE',
  'REPORT_SERVICE',
  'ESEMINAR',
  'NEW_HIRE_TUTORIAL',
].forEach((c) => {
  test(`Is ${c} component up and running`, async (t) => {
    const response = await GET('/frbweb/fhrnavigator/status/FEMA');
    await t.expect(response.statusCode).eql(200);
    const json = JSON.parse(response.rawData);
    await t.expect(status(c, json)).eql(['UP']);
  });
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const status = (name: string, json: any) => {
  return JSONPath({
    path: `$.component[?(@.name == "${name}")].status`,
    json,
  });
};
